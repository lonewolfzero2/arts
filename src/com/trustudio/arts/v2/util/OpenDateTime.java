package com.trustudio.arts.v2.util;

import java.util.Calendar;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenDateTime untuk mengatur tanggal mendapatkan tanggal sekarang
 * mendapatkan bulan dari format tanggal.
 */
public class OpenDateTime {

	public static int getCurrentHour() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	public static int getCurrentMinute() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.MINUTE);
	}

	public static String getTimeString(int time) {
		if (time < 10) {
			return "0" + time;
		} else {
			return String.valueOf(time);
		}
	}

	public static Calendar getCalendar(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		return cal;
	}

	public static String getMonth(String data) {
		String temp = "";
		int bulan = Integer.parseInt(data.substring(4, 6));
		System.out.println("Bulan : " + bulan);
		switch (bulan) {
		case 1:
			temp = "Januari ";
			break;
		case 2:
			temp = "Februari ";
			break;
		case 3:
			temp = "Maret ";
			break;
		case 4:
			temp = "April ";
			break;
		case 5:
			temp = "Mei ";
			break;
		case 6:
			temp = "Juni ";
			break;
		case 7:
			temp = "Juli ";
			break;
		case 8:
			temp = "Agustus ";
			break;
		case 9:
			temp = "September ";
			break;
		case 10:
			temp = "Oktober ";
			break;
		case 11:
			temp = "November ";
			break;
		case 12:
			temp = "Desember ";
			break;
		}
		temp += data.substring(0, 4);
		return temp;
	}

	public static String getIndonesianDay(int day) {
		String[] hari = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" };
		return hari[day - 1];
	}

}
