package com.trustudio.arts.v2.util;

import org.json.JSONArray;
import org.json.JSONObject;

import com.trustudio.arts.v2.model.ConditionModel;

import android.util.Log;

public class ConditionParser {

	private final static String TAG = "ConditionParser";

	public static boolean parse(JSONObject condition, JSONObject answer) {

		Log.d("answer at parser", answer.toString());
		boolean con = false;
		String operator = null;

		try {
			if (condition.has(QuestionCons.OPERATOR)) {
				operator = condition.getString(QuestionCons.OPERATOR);

				/* if json has OPERATOR key then it must be array */
				JSONArray arg = condition.getJSONArray(QuestionCons.KONDISI);
				for (int i = 0; i < arg.length(); i++) {

					if (arg.get(i) instanceof JSONObject) {

						Log.d("con after parse again", con + "");
						if (operator == null || i == 0) {
							con = parse(arg.getJSONObject(i), answer);
						} else {
							con = conditionCheck(operator, con, parse(arg.getJSONObject(i), answer));
						}

					} else if (arg.get(i) instanceof String) {
						if (operator == null || i == 0) {
							con = conditionCheck(arg.getString(i), answer);
						} else {
							con = conditionCheck(con, operator, arg.getString(i), answer);
						}
						Log.d("con at operator " + operator + " index " + i, con + "");
					} else {
						Log.e("not string nor json", "you should not come here");
					}
				}
			} else {
				/* if not, then it must be one condition */

				String one = condition.getString(QuestionCons.CONDITION);
				con = conditionCheck(one, answer);
			}

		} catch (Exception e) {
			Log.d(TAG, e.toString());
			return false;
		}
		return con;
	}

	private static boolean conditionCheck(String operator, boolean currentBoolean, boolean newBoolean) {
		if (operator.equalsIgnoreCase("OR")) {
			return currentBoolean || newBoolean;
		} else if (operator.equalsIgnoreCase("AND")) {
			return currentBoolean && newBoolean;
		} else {
			return false;
		}
	}

	/* First Check */
	private static boolean conditionCheck(boolean con, String operator, String raw, JSONObject answer) {
		if (operator.equalsIgnoreCase("OR")) {
			return con || conditionCheck(raw, answer);
		} else if (operator.equalsIgnoreCase("AND")) {
			return con && conditionCheck(raw, answer);
		}
		return false;
	}

	/* Condition check */
	private static boolean conditionCheck(String raw, JSONObject answer) {
		try {
			ConditionModel cm = makeModel(raw);

			if (cm.parentVar != null) {
				if (answer.has(cm.parentVar)) {
					answer = answer.getJSONObject(cm.parentVar);
				}
			}

			if (answer.has(cm.var)) {
				if (answer.getString(cm.var).equalsIgnoreCase(cm.value))
					return true;
				else
					return false;
			}

			Log.e("answer not found", "you shouldnt come here, INFO :" + cm.var + "VAL:" + cm.value);
			return false;

		} catch (Exception e) {
			Log.e(TAG + " conditionCheck", e.toString());
			return false;
		}
	}

	/* Split and make a model that distinguish identifier and condition */
	/* example : j0001=true output : var = j0001 value=true */

	private static ConditionModel makeModel(String raw) {
		String[] rawArray = raw.split("=");
		return new ConditionModel(rawArray[0].trim(), rawArray[1].trim());
	}
}
