package com.trustudio.arts.v2.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.*;

import com.trustudio.arts.v2.model.ConfigModel;

public class Utils {
	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static ConfigModel fastDecodeConfig(ConfigModel model) {

		try {
			String provider = URLDecoder.decode(model.getProvider(), URLCons.UTF_8);
			//String pekerjaan = URLDecoder.decode(model.getJob(), URLCons.UTF_8);
			String dokumen = URLDecoder.decode(model.getDokumen(), URLCons.UTF_8);
			String siteCode = URLDecoder.decode(model.getSiteCode(), URLCons.UTF_8);

			return new ConfigModel(provider, siteCode, dokumen);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean isInternetOn(ContextWrapper context){
		ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		/* If connected to the net */
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ){
			return true;
		
		/* If not connected to the net */
		}else if(connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED || 
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED){
			return false;
		}
		
		return false;
	}
}
