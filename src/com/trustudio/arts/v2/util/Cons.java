package com.trustudio.arts.v2.util;

public class Cons {
	public static final String TAG = "ARTS";	
	
	//api url & path
	public static final String API_URL = "http://3trust.com/arts_beta/web/dokumen/api";
	public static final String API_URL_JenisDokumen = "http://3trust.com/arts_beta/web/JenisDokumen/api/json";
	public static final String API_URL_site = "http://3trust.com/arts_beta/web/site/api/json";
	
	//http://3trust.com/arts_beta/web/dokumen/api/save?sitecode=testing001&dokumen=TESTING&provider=TRI
	//http://3trust.com/nexwave/tower2/web/dokumen/api
	
	//database name & path
	public static final String DBNAME = "arts.db";
	public static final String DBPATH = "/data/data/com.trustudio.arts.v2/";
	public static final String APP_DIR = "/.arts";
	public static final String APP_DIR_OPEN = "/Nexwave";
	
	public static final String PACKAGE_PREFIX = "com.arts";
	public static final int DB_VERSION = 2;
	
	//private preferences key name
	public static final String PRIVATE_PREF = "hcis_pref";
	public static final String DBVER_KEY = "dbversion";
	public static final String APPVER_KEY = "appversion";
	
	//enable debug
	public static final boolean ENABLE_DEBUG = true;

	//Http connection timeout
	public static final int HTTP_CONNECTION_TIMEOUT = 20000;
	
	//
	public static final int SPLASHSCREEN_TIMEOUT = 1000;
}
