package com.trustudio.arts.v2.model;

public class ConditionModel {

	private static final String SPLITTER = "_";

	public String var;
	public String value;
	public String parentVar;

	public ConditionModel() {
		// TODO Auto-generated constructor stub
	}

	public ConditionModel(String var, String value) {
		this.var = var;
		this.value = value;

		if (this.var.contains(SPLITTER)) {
			this.parentVar = this.var.substring(0, var.indexOf(SPLITTER));
		}

	}
}
