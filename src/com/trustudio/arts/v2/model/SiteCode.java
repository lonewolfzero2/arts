package com.trustudio.arts.v2.model;

public class SiteCode {

	public static final String _DATA = "data";

	public static final String ID = "Id";
	public static final String SITE_CODE = "SiteCode";
	public static final String VENDOR_ID = "VendorId";
	public static final String VENDOR_ID_STRING = "VendorId_toString";
	public static final String PROVIDER_ID = "ProviderId";
	public static final String PROVIDER_ID_STRING = "ProviderId_toString";
	public static final String AREA_ID = "AreaId";
	
	public String siteid;

	private int id, vendorID, providerID, areaID;
	private String siteCode, vendorIDString, providerIDString;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVendorID() {
		return vendorID;
	}

	public void setVendorID(int vendorID) {
		this.vendorID = vendorID;
	}

	public int getProviderID() {
		return providerID;
	}

	public void setProviderID(int providerID) {
		this.providerID = providerID;
	}

	public int getAreaID() {
		return areaID;
	}

	public void setAreaID(int areaID) {
		this.areaID = areaID;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getVendorIDString() {
		return vendorIDString;
	}

	public void setVendorIDString(String vendorIDString) {
		this.vendorIDString = vendorIDString;
	}

	public String getProviderIDString() {
		return providerIDString;
	}

	public void setProviderIDString(String providerIDString) {
		this.providerIDString = providerIDString;
	}

}
