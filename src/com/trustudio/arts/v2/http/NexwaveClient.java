package com.trustudio.arts.v2.http;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import com.trustudio.arts.v2.util.URLCons;

import android.content.Context;
import android.util.Log;


public class NexwaveClient {

	public static final String STATUS = "status";
	public static final String STATUS_OK = "ok";

	private final static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void get(Context context, String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		client.get(context, getAbsoluteUrl(url), params, responseHandler);
	}

	public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		Log.d("request", url);
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void post(Context context, String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		Log.d("request", url);
		client.post(context, getAbsoluteUrl(url), params, responseHandler);
	}

	public static void cancel(Context context) {
		client.cancelRequests(context, true);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return URLCons.BASE_URL + relativeUrl;
	}

	public String MakeRequest(String url, String json) {
		JSONObject j = null;
		try {
			j = new JSONObject(json);
		} catch (Exception e) {
			Log.e("errror", e.toString());
		}

		HttpParams params = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 50000;
		HttpConnectionParams.setSoTimeout(params, timeoutSocket);
		HttpClient client = new DefaultHttpClient(params);

		HttpResponse response = doPost(j, url, (DefaultHttpClient) client);

		if (response == null) {
			return "e";
		}
		return getRespone(response);
	}

	private HttpResponse doPost(JSONObject json, String url, DefaultHttpClient client) {
		org.apache.http.HttpResponse mResponse = null;
		try {
			// define http post
			//HttpHost host = new HttpHost("www.3trust.com", 80, "http");
			HttpPost post = new HttpPost(url);

			// passes the results to a string builder/entity
			// sets the post request as the resulting string
			StringEntity entity = new StringEntity(json.toString());

			post.setEntity(entity);
			post.getParams().setParameter(" ", json);

			/* Some Credential */
			// client.getCredentialsProvider().setCredentials(new
			// AuthScope(host.getHostName(), host.getPort()),
			// new UsernamePasswordCredentials(AUTH_USERNAME, AUTH_PASSWORD));

			// Post the data
			post.setHeader("Accept", "application/json");
			post.setHeader("Content-Type", "application/json");
			// Execute
			mResponse = client.execute(post);
			return mResponse;
		} catch (Exception e) {
			Log.e("doPost", e.toString());
			return null;
		}
	}

	private String getRespone(HttpResponse response) {
		StringBuilder str = new StringBuilder();
		if (response != null) {
			String line = null;
			try {
				InputStream in = response.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str.toString();
	}

}
