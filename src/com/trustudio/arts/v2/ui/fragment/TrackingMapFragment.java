package com.trustudio.arts.v2.ui.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.db.MasterDb;
import com.trustudio.arts.v2.ui.CandidateLocationActivity;
import com.trustudio.arts.v2.ui.TrackingUploadActivity;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.ImageHelper;
import com.trustudio.arts.v2.util.StorageManager;

public class TrackingMapFragment extends BaseSupportMapFragment implements LocationListener {
	private Context context;
	private ArrayList<LatLng> trackList;
	private LocationManager mLocManager;
	
	private LatLng position;
	private Marker oldPosition;
	private Marker currentPosition;
	private Marker pointPosition;
	private TextView statusPhoto;
	private EditText caption;
	
	private MasterDb mMasterDb;
	
	private onTrackingListener mTrackingListener;

	private Timer timerTracker = new Timer();

    private int UPDATE_INTERVAL = 1000;
	private boolean mIsGpsRunning = false;
	private boolean mIsTracking = false;
	
	private String siteID;
	private String provider;
	private String dokumen;
	private File path = null;
	
	private int count = 0;

	public interface onTrackingListener{
		public void setButtonTrackEnable(boolean enabled);
		public void setButtonCaptureEnable(boolean enabled);
		public void setButtonFinishEnable(boolean enabled);
		public void setButtonTrackText(int text);
		public void setButtonTrackOnClickListener(OnClickListener OnClickLister);
		public void setButtonCaptureOnClickListener(OnClickListener OnClickLister);
		public void setButtonFinishOnClickListener(OnClickListener OnClickLister);
	}
	
	public static TrackingMapFragment newInstance(String provider, String dokumen, String siteID) {
		TrackingMapFragment trackMapFrag = new TrackingMapFragment();
		
		Debug.i(siteID);
		Debug.i(provider);
		Debug.i(dokumen);
		
		Bundle bundle 	= new Bundle();
		bundle.putString("SiteID", siteID);
		bundle.putString("provider", provider);
		bundle.putString("dokumen", dokumen);
		
		trackMapFrag.setArguments(bundle);
		return trackMapFrag;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context				= activity;
		mTrackingListener	= (onTrackingListener) activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		siteID	= getArguments().getString("SiteID");
		provider= getArguments().getString("provider");
		dokumen	= getArguments().getString("dokumen");
		
		/*path 	= Environment.getExternalStorageDirectory() + Cons.APP_DIR_OPEN 
				+ "/Photo/Tracking/" + siteID + "/" + "ss" + siteID + ".png";*/
		
		mLocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	
		mMasterDb	= new MasterDb(getDatabase());
		trackList 	= new ArrayList<LatLng>();

		mTrackingListener.setButtonFinishEnable(false);
		mTrackingListener.setButtonCaptureEnable(false);
		mTrackingListener.setButtonTrackEnable(false);
		mTrackingListener.setButtonTrackText(R.drawable.ic_menu_play_clip);
		mTrackingListener.setButtonTrackOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(mIsTracking)
					stopTracking();
				else
					startTracking();
			}
		});
		
		mTrackingListener.setButtonCaptureOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				showDialogCapture();
			}
		});
		
		mTrackingListener.setButtonFinishOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(context, TrackingUploadActivity.class);
				
				intent.putExtra("com.siteid", siteID);
				intent.putExtra("com.provider", provider);
				intent.putExtra("com.dokumen", dokumen);
				
				startActivity(intent);
				
				getActivity().finish();
			}
		});
		
		getLastKnownLocations();
	}
    
    @Override
    public void onPause() {
    	Debug.i("OnPause");
    	
    	timerTracker.cancel();
    	
    	super.onPause();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	
    	mMasterDb.reload(getDatabase());
    	
    	Debug.i("OnResume");
    }
    
    @Override
    public void onDestroy() {
    	Debug.i("OnDestroy");
    	
    	if (mIsGpsRunning) mLocManager.removeUpdates(this);
    	
    	super.onDestroy();
    }
    
	public void CaptureMapScreen(){
		SnapshotReadyCallback callback = new SnapshotReadyCallback(){
			Bitmap bitmap;
			
			@Override
			public void onSnapshotReady(Bitmap snapshot) {
				bitmap = snapshot;
				
				try {
					File f = StorageManager.getImageFile("/" + provider + "/" + dokumen + "/" + siteID + "/" + "Tracking/", "screenshot_tracking");
					FileOutputStream out = new FileOutputStream(f);
					bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
					showDialogMessage("Sukses", "Screenshot disimpan di " + f);
					
					mMasterDb.insert(provider, dokumen, siteID, "tracking_screenshot", "", f.toString(), String.valueOf(0));
				} catch (Exception e) {
					e.printStackTrace();
					showDialogMessage("Oops", "Screenshot gagal");
				}
			}
		};
		
		getMap().snapshot(callback);
	}
    
    private void startTracking(){
    	mIsTracking = true;
    	mTrackingListener.setButtonFinishEnable(false);
    	mTrackingListener.setButtonCaptureEnable(false);
    	mTrackingListener.setButtonTrackText(R.drawable.ic_menu_stop);
    	
    	timerTracker = new Timer();
        TimerTask timerTask = new TrackingTimerTask();
        timerTracker.scheduleAtFixedRate(timerTask, 0, UPDATE_INTERVAL);
    }
    
    private void stopTracking(){
    	mIsTracking = false;
    	mTrackingListener.setButtonFinishEnable(true);
    	mTrackingListener.setButtonCaptureEnable(true);
    	mTrackingListener.setButtonTrackText(R.drawable.ic_menu_play_clip);
    	
    	timerTracker.cancel();
    }
    
    private void getCurrentLocation(){
    	if (mIsGpsRunning) return;
		
		try {
			if (!mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS); 
				
				startActivity(intent);
			} else {
				mIsGpsRunning = true;
				mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
			}
		} catch (Exception ex) {
			showToast(R.string.text_error_gps);
		}
    }
    
    private void getLastKnownLocations(){
    	Criteria cri		= new Criteria();
    	String bbb 			= mLocManager.getBestProvider(cri, true);
    	Location myLocation = mLocManager.getLastKnownLocation(bbb);

    	try {
        	double lat	= myLocation.getLatitude();
        	double lon 	= myLocation.getLongitude();
        	LatLng ll = new LatLng(lat, lon);
        	
        	oldPosition = setMarkerLocation(ll, R.drawable.ic_maps_indicator_old_position);
    		getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 17));
            getMap().animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	getCurrentLocation();
    }
    
	public void showDialogCapture() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		
		View	view	= LayoutInflater.from(context).inflate(R.layout.dialog_capture, null);
		Button	capture	= (Button) view.findViewById(R.id.capture);
		statusPhoto		= (TextView) view.findViewById(R.id.status_photo);
		caption			= (EditText) view.findViewById(R.id.input);
		
		capture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				path = null;
				capture();
			}
		});
		
		builder.setTitle("POINT");
		builder.setView(view);
		builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
		  	  		@Override
		  	  		public void onClick(DialogInterface dialog, int which) {
		  	  			if(path != null){
			  	  			LatLng position = new LatLng(trackList.get(trackList.size()-1).latitude, trackList.get(trackList.size()-1).longitude);
			  	  			pointPosition 	= addMarker(position, R.drawable.ic_maps_indicator_old_position, caption.getText().toString());
			  	  		
			  	  			mMasterDb.insert(provider, dokumen, siteID, caption.getText().toString(), position.toString(), path.toString(), String.valueOf(count));
		  	  			
			  	  			dialog.dismiss();
		  	  			}else{
		  	  				showToast("Gagal... Path null! atau anda tidak mengisi caption");
		  	  			}
		  	  			
		  	  		}
		  	    });
		
		builder.create().show();
	}

	@Override
	public void onLocationChanged(Location location) {
		position = new LatLng(location.getLatitude(), location.getLongitude());
		
		if(oldPosition != null)
			oldPosition.remove();	
		
		if(currentPosition == null){
			mTrackingListener.setButtonTrackEnable(true);
			mTrackingListener.setButtonCaptureEnable(false);
			
			getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(position, 17));
	        getMap().animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
		}else{
			currentPosition.remove();
		}
		
		currentPosition = setMarkerLocation(position, R.drawable.ic_maps_indicator_current_position);
	}

	@Override
	public void onProviderDisabled(String arg0) {}

	@Override
	public void onProviderEnabled(String arg0) {}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}
	
	public class TrackingTimerTask extends TimerTask {
	    private Handler mHandler = new Handler();

	    @Override
	    public void run() {
	        new Thread(new Runnable() {
	            @Override
	            public void run() {
	                mHandler.post(new Runnable() {
	                    @Override
	                    public void run() {
	                    	PolylineOptions plo = new PolylineOptions();
	                    	plo.color(Color.MAGENTA);
	                    	plo.width(3);
	                    	
	                        trackList.add(position);	
	                    	
	                    	plo.addAll(trackList);
	                    	
	                    	getMap().addPolyline(plo);
	                    	
	                    	Debug.i(String.valueOf(position));

	                    	//showToast("Tracking mode " + position);
	                    }
	                });
	            }
	        }).start();
	    }
	}
	
	private void capture(){
		String[] items = { "Take From Camera", "Select From Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int pos) {
				Intent intent = null;
				switch (pos) {
				case 0:
					intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra("outputX", 250);
					intent.putExtra("outputY", 250);
					intent.putExtra("return-data", true);
					break;
				case 1:
					intent = new Intent(Intent.ACTION_PICK);
					intent.setType("image/*");
					intent.putExtra("outputX", 250);
					intent.putExtra("outputY", 250);
					intent.putExtra("return-data", true);
					break;
				}
				
				int requestCode = 1;
				
				startActivityForResult(intent, requestCode);
				
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			if (requestCode == 1) {
				try {
					Bitmap bmp;

					boolean isFromExtra = data.getExtras() == null ? false : true;
					if (isFromExtra) {
						bmp = (Bitmap) data.getExtras().get("data");
					} else {
						bmp = ImageHelper.getBitmapFromUri(context, data.getData());
						float degree = ImageHelper.rotationForImage(context, data.getData());
						bmp = ImageHelper.rotate(bmp, (int) degree);
					}

					try {
						//new File(questionHelper.answer.getString(img.getTag().toString())).delete();
					} catch (Exception e) {
						Log.w("get photo path", e.toString());
					}
					
					statusPhoto.setText("Photo OK");
					
					count = count + 1;
					path = StorageManager.getImageFile("/" + provider + "/" + dokumen + "/" + siteID + "/" + "Tracking/", "tracking_foto_" + count);
					FileOutputStream out = new FileOutputStream(path);
					bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
					
					//mPhotoNominal.setImageBitmap(bmp);

					//questionHelper.inputData(img.getTag().toString(), f.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("get imageview", e.toString());
					statusPhoto.setText("Photo Gagal");
				}
			}
		}
	}
}
