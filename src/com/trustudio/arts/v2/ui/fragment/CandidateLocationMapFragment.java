package com.trustudio.arts.v2.ui.fragment;

import java.net.URL;
import java.util.ArrayList;

import org.w3c.dom.Document;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.GMapV2Direction;

public class CandidateLocationMapFragment extends BaseSupportMapFragment implements LocationListener{
	private Document document;

	private Context context;
	private LocationManager mLocManager;
	private GetDirections mGetDirections;

	private Marker oldPosition;
	private Marker currentPosition;
	private ProgressDialog mProgressLocation;
	private ProgressDialog mProgressDirections;
	
	private boolean mIsGpsRunning = false;
	private boolean mIsLocationsLoading = false;
	private boolean mIsDirectionsLoading = false;
	
	public static CandidateLocationMapFragment newInstance() {
		return new CandidateLocationMapFragment();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context	= activity;
	}
	
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	  
    	mLocManager 		= (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    	
    	mProgressLocation	= new ProgressDialog(context);
    	
    	getLastKnownLocations();
    }
    
    @Override
    public void onPause() {
    	Debug.i("OnPause");
    	
    	if(mIsDirectionsLoading && mGetDirections != null)
    		mGetDirections.cancel(true);

    	super.onPause();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	
    	Debug.i("OnResume");
    }
    
    @Override
    public void onDestroy() {
    	Debug.i("OnDestroy");
    	
    	if (mIsGpsRunning) mLocManager.removeUpdates(this);
    	
    	super.onDestroy();
    }
    
    private void getCurrentLocation(){
    	if (mIsGpsRunning) return;
		
		try {
			if (!mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS); 
				
				startActivity(intent);
			} else {
				mIsGpsRunning = true;
				mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
				
		    	// Set progress for getting location
		    	mIsLocationsLoading = true;
				mProgressLocation.setMessage(getResources().getText(R.string.text_getting_locations));
				mProgressLocation.show();
				mProgressLocation.setCanceledOnTouchOutside(false);
			}
		} catch (Exception ex) {
			showToast(R.string.text_error_gps);
		}
    }
    
    private void getLastKnownLocations(){
    	Criteria cri		= new Criteria();
    	String bbb 			= mLocManager.getBestProvider(cri, true);
    	Location myLocation = mLocManager.getLastKnownLocation(bbb);

    	try {
    		double lat	= myLocation.getLatitude();
        	double lon 	= myLocation.getLongitude();
        	LatLng ll = new LatLng(lat, lon);
        	
        	oldPosition = setMarkerLocation(ll, R.drawable.ic_maps_indicator_old_position);
    		getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 17));
            getMap().animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	getCurrentLocation();
    }

	@Override
	public void onLocationChanged(Location location) {
		LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
		
		if(oldPosition != null)
			oldPosition.remove();	
		
		if(currentPosition != null)
			currentPosition.remove();

		currentPosition = setMarkerLocation(position, R.drawable.ic_maps_indicator_current_position);
		
		if(mIsLocationsLoading){
			mIsLocationsLoading = false;
			mProgressLocation.dismiss();
			
			getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(position, 17));
	        getMap().animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
	        
	    	(mGetDirections = new GetDirections(position)).execute();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}
	
	private class GetDirections extends AsyncTask<URL, Integer, Long>{
		private GMapV2Direction gMapDirection;
		private LatLng location;
		
		public GetDirections(LatLng location) {
			this.location = location;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			mProgressDirections	= new ProgressDialog(getActivity());
			gMapDirection 	= new GMapV2Direction();
			
			mIsDirectionsLoading = true;
			mProgressDirections.setMessage(getResources().getString(R.string.text_getting_directions));
			mProgressDirections.show();
			mProgressDirections.setCanceledOnTouchOutside(false);
		}
		
		@Override
		protected Long doInBackground(URL... params) {
			long result = 0;
			
			try {
				LatLng fromPosition = location;
				LatLng toPosition 	= new LatLng(-6.887956, 107.555455);
				
				document = gMapDirection.getDocument(fromPosition, toPosition, GMapV2Direction.MODE_DRIVING);
				
				result = 1;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return result;
		}
		
		@Override
		protected void onPostExecute(Long result) {
			super.onPostExecute(result);
			
			mIsDirectionsLoading = false;
			mProgressDirections.dismiss();
			
			if(result == 1 && document != null){
				ArrayList<LatLng> directionPoint = gMapDirection.getDirection(document);
				
				PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);

				for(int i = 0 ; i < directionPoint.size() ; i++) {          
					rectLine.add(directionPoint.get(i));
				}
				
				getMap().addPolyline(rectLine);
			}else {
				showDialogMessage("Opps!", "Gagal mengambil Arah");
			}
		}
	}
}
