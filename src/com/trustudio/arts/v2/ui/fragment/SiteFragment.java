package com.trustudio.arts.v2.ui.fragment;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.http.ArtsConnection;
import com.trustudio.arts.v2.model.SiteCode;
import com.trustudio.arts.v2.ui.SiteListActivity;
import com.trustudio.arts.v2.util.Debug;

public class SiteFragment extends BaseFragment{
	private Context context;
	
	private boolean mDurationLoading = false;
	private ProgressDialog mProgressDialogDuration;
	private ArtsConnection conn;
	private ArrayList<SiteCode> mList;
	private JenisDocumentTask mTask;
	private AutoCompleteTextView tSite;
	
	public static SiteFragment newInstance(String jenis_dokumen) {
		SiteFragment site_fragment = new SiteFragment();
		
		Bundle bundle = new Bundle();
		bundle.putString("jenis_dokumen", jenis_dokumen);
		
		site_fragment.setArguments(bundle);
		
		return site_fragment;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context			= activity;
		//mSiteListener	= (OnSiteListener) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_site, null);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		Button bSelect	= (Button) getView().findViewById(R.id.select);
		tSite			= (AutoCompleteTextView) getView().findViewById(R.id.site_id);

		conn					= new ArtsConnection();
		mProgressDialogDuration = new ProgressDialog(context);
		
		final String jd = getArguments().getString("jenis_dokumen");
		
		Debug.i(jd);
		
		bSelect.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if(tSite.getText().toString().equals("")){
					showToast("Site ID tidak boleh kosong!");
				}else{
					Intent intent = new Intent(context, SiteListActivity.class);
						
						String jd = getArguments().getString("jenis_dokumen");
						
						intent.putExtra("com.site", tSite.getText().toString());
						intent.putExtra("com.jenis_dokumen", jd);
						
						startActivity(intent);
					
				}
				
				//mSiteListener.onSubmitCLickLIstener();
			}
		});
		
		(mTask = new JenisDocumentTask()).execute();
	}
	
	@Override
	public void onPause() {
		if(mDurationLoading && mTask != null)
			mTask.cancel(true);
			
		super.onPause();
	}

	private class JenisDocumentTask extends AsyncTask<URL, Integer, Long>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			mDurationLoading = true;
			progressIntermediate(true);
			mProgressDialogDuration.setMessage("Meminta Site Id....");
			mProgressDialogDuration.show();
			mProgressDialogDuration.setCanceledOnTouchOutside(false);
		}
		
		@Override
		protected Long doInBackground(URL... arg0) {
			long result = 0;
			
			try {
				mList = conn.getSite();
				result = 1;
			} catch (Exception e) {
				mDurationLoading = false;
				e.printStackTrace();
			}
			
			return result;
		}
		
		@Override
		protected void onPostExecute(Long result) {
			super.onPostExecute(result);
			mDurationLoading = false;
			progressIntermediate(false);
			mProgressDialogDuration.dismiss();
			
			if(result == 1){
				ArrayList<String> arr = new ArrayList<String>();
				
				for(int i=0;i<mList.size();i++){
					arr.add(mList.get(i).siteid);
				}
				
				String[] item = arr.toArray(new String[arr.size()]);
				
				
				ArrayAdapter<String> adapter 	= new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, item);
		        
		        tSite.setAdapter(adapter);
			}else{
				showToast("Request data gagal...");
			}
		}
	}
	
	public interface OnSiteListener{
		void onSubmitCLickLIstener();
	}
}
