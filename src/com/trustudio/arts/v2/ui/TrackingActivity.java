package com.trustudio.arts.v2.ui;

import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.ui.fragment.TrackingMapFragment;
import com.trustudio.arts.v2.ui.fragment.TrackingMapFragment.onTrackingListener;

public class TrackingActivity extends BaseActivity implements onTrackingListener{
	private TrackingMapFragment mFragmentMapSupport;
	
	private ImageView mTracking;
	private ImageView mCapture;
	private Button mFinish;
	
	private String siteID;
	private String provider;
	private String dokumen;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tracking);
		
		getSupportActionBar().setTitle(R.string.text_tracking);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setSupportProgressBarIndeterminateVisibility(false);
		
		Bundle bundle 	= getIntent().getExtras();
		siteID			= bundle.getString("com.siteid");
		provider		= bundle.getString("com.provider");
		dokumen			= bundle.getString("com.dokumen");
		
		mTracking		= (ImageView) findViewById(R.id.tracking);
		mCapture		= (ImageView) findViewById(R.id.capture);
		mFinish			= (Button) findViewById(R.id.finish);
		
		if(savedInstanceState == null){
			mFragmentMapSupport = TrackingMapFragment.newInstance(provider, dokumen, siteID);
	
			getSupportFragmentManager().beginTransaction().add(R.id.map, mFragmentMapSupport).commit();
		}
	}

	@Override
	public void setButtonTrackEnable(boolean enabled) {
		mTracking.setEnabled(enabled);
	}


	@Override
	public void setButtonCaptureEnable(boolean enabled) {
		mCapture.setEnabled(enabled);
	}

	@Override
	public void setButtonFinishEnable(boolean enabled) {
		mFinish.setEnabled(enabled);
	}

	@Override
	public void setButtonTrackOnClickListener(OnClickListener OnClickLister) {
		mTracking.setOnClickListener(OnClickLister);
	}

	@Override
	public void setButtonCaptureOnClickListener(OnClickListener OnClickLister) {
		mCapture.setOnClickListener(OnClickLister);
	}

	@Override
	public void setButtonFinishOnClickListener(OnClickListener OnClickLister) {
		mFinish.setOnClickListener(OnClickLister);
	}

	@Override
	public void setButtonTrackText(int text) {
		mTracking.setImageResource(text);		
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		menu.add("Screenshot")
		.setIcon(R.drawable.ic_menu_camera)
		.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				try {
					mFragmentMapSupport.CaptureMapScreen();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		})
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		return super.onCreateOptionsMenu(menu);
	}
		
}
