package com.trustudio.arts.v2.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.http.AsyncHttpResponseHandler;
import com.trustudio.arts.v2.http.NexwaveClient;
import com.trustudio.arts.v2.http.RequestParams;
import com.trustudio.arts.v2.model.ConfigModel;
import com.trustudio.arts.v2.model.QuestionData;
import com.trustudio.arts.v2.model.ViewDataHolder;
import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.FingerPaint;
import com.trustudio.arts.v2.util.FormBuilder;
import com.trustudio.arts.v2.util.ImageHelper;
import com.trustudio.arts.v2.util.OpenData;
import com.trustudio.arts.v2.util.QuestionCons;
import com.trustudio.arts.v2.util.RequestConstant;
import com.trustudio.arts.v2.util.StorageManager;
import com.trustudio.arts.v2.util.URLCons;
import com.trustudio.arts.v2.util.Utils;

public class FormViewerActivity extends BaseActivity implements OnClickListener {

	public final static String DIR = "DIR";

	private QuestionData questionHelper;
	private JSONArray data;
	private FormBuilder formBuilder;

	private String mSubDir;
	private int templateID;

	private LinearLayout lyContainer;
	
	private String siteID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_viewer);

		lyContainer = (LinearLayout) findViewById(R.id.lyContainer);
		
		Bundle bundle = getIntent().getExtras();
		siteID		  = bundle.getString(ConfigModel.SITE_CODE);
		
		/* set title pada action bar */
		initHeader();

		formBuilder 	= new FormBuilder(this);
		questionHelper 	= new QuestionData(this);

		//sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(Environment.getExternalStorageDirectory() + Cons.APP_DIR_OPEN)));
		
		new LoadTask().execute();
	}

	class LoadTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog pgLoading = new ProgressDialog(FormViewerActivity.this);

		protected void onPreExecute() {
			pgLoading = new ProgressDialog(FormViewerActivity.this);
			pgLoading.setMessage("Parsing data...");
			pgLoading.show();
		};

		@Override
		protected Void doInBackground(Void... params) {

			Bundle b = getIntent().getExtras();
			if (b != null) {
				mSubDir = b.getString(DIR);
				templateID = b.getInt(QuestionCons.TEMPLATE_DOC_ID);
			}

			data = questionHelper.getQuestionAtId(templateID);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			lyContainer.addView(renderForm(0, data.length()));

			pgLoading.dismiss();
		}
	};

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		getSupportMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemID = item.getItemId();

		switch (itemID) {
		case android.R.id.home:
			confirmDialog();
			break;
		case R.id.menuSave:
			saveAnswer();
			break;
		case R.id.menuSend:
			onSend();
			break;
		/* Tambahan Si Gue 
		case R.id.menuCek:
			checkForm(0, data.length());
			break;*/
		}

		return super.onOptionsItemSelected(item);
	}

	public QuestionData getData() {
		return questionHelper;
	}

	public void onGetAnswer(View v) {
		Toast.makeText(this, questionHelper.getAnswer().toString(), Toast.LENGTH_LONG).show();
	}

	/** Return LinearLayout yang berisi form yang sudah di render **/
	private LinearLayout renderForm(final int offset, final int limit) {

		final LinearLayout llVertical = formBuilder.getContainer();

		new AsyncTask<Void, Void, Void>() {

			Handler handler = new Handler();

			private ProgressDialog pd = new ProgressDialog(FormViewerActivity.this);

			protected void onPreExecute() {
				pd.setMessage("Rendering form..");
				pd.show();
			};

			@Override
			protected Void doInBackground(Void... params) {
				Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

				try {
					for (int x = offset; x < limit + 1; x++) {

						final JSONObject json = data.getJSONObject(x);
						final int type = json.getInt(QuestionCons.ANSWER_TYPE_ID);
						final String varName = json.getString(QuestionCons.VARIABLE);

						Debug.i("CEK ANSWER!!!" + json.toString());
						
						String answer = OpenData.getAnswerData(getApplication(), templateID);
						JSONObject savedAnswer = null;
						if (answer != null) {
							savedAnswer = new JSONObject(answer);
						}

						/* Input saved answer if there any */
						if (savedAnswer != null && savedAnswer.has(varName)) {
							questionHelper.inputData(varName, savedAnswer.getString(varName));
						} else {
							/** Create Answer JSON with empty value */
							questionHelper.inputData(varName, "");
						}

						/** Save Condition JSON for later use */
						if (json.has(QuestionCons.CONDITION)) {

							if (json.get(QuestionCons.CONDITION) instanceof JSONObject
									&& json.get(QuestionCons.CONDITION) != JSONObject.NULL) {
								questionHelper.inputCondition(json.getString(QuestionCons.VARIABLE),
										json.getJSONObject(QuestionCons.CONDITION));
							} else {
								questionHelper.inputCondition(json.getString(QuestionCons.VARIABLE),
										json.getString(QuestionCons.CONDITION));
							}
						}

						handler.post(new Runnable() {

							@Override
							public void run() {

								View v = null;

								if (type == QuestionCons.TYPE_TEXT) {
									v = formBuilder.createTextType(json);
								} else if (type == QuestionCons.TYPE_MULTI_RADIO) {
									v = formBuilder.createMultipleChoiceType(json, formBuilder.ocl);
								} else if (type == QuestionCons.TYPE_GPS_LOC) {
									v = formBuilder.createGpsLocationForm(json);
								} else if (type == QuestionCons.TYPE_PHOTO) {
									v = formBuilder.createPhotoForm(json);
								} else if (type == QuestionCons.TYPE_YES_NO) {
									v = formBuilder.createYesNoType(json, formBuilder.ocl);
								} else if (type == QuestionCons.TYPE_OK_NOK) {
									v = formBuilder.createOkNokType(json, formBuilder.ocl);
								} else if (type == QuestionCons.TYPE_OK_NOK_NA) {
									v = formBuilder.createOkNokNaType(json, formBuilder.ocl);
								} else if (type == QuestionCons.TYPE_PASSWORD) {
									v = formBuilder.createPasswordType(json);
								} else if (type == QuestionCons.TYPE_DECIMAL) {
									v = formBuilder.createDecimalType(json);
								} else if (type == QuestionCons.TYPE_INTEGER) {
									v = formBuilder.createIntegerType(json);
								} else if (type == QuestionCons.TYPE_MULTI_SELECT) {
									v = formBuilder.createMultiSelectType(json); /** tambahan: menghapus listener pada method createMultiSelectType **/
								} else if (type == QuestionCons.TYPE_SIGNATURE) {
									v = formBuilder.createSignatureType(json);
								} else if (type == QuestionCons.TYPE_TIME) {
									v = formBuilder.createTimeType(json);
								} else if (type == QuestionCons.TYPE_DATE) {
									v = formBuilder.createDateType(json);
								} else if (type == QuestionCons.TYPE_ATTACHMENT) {
									v = formBuilder.createAttachmentType(json);
								} else if (type == QuestionCons.TYPE_DATE_TIME) {
									v = formBuilder.createDateTimeType(json);
								} else if (type == QuestionCons.TYPE_CODE_SCAN) {
									v = formBuilder.createQRCodeType(json);
								} else if (type == QuestionCons.TYPE_GROUP_TEXT) {
									v = formBuilder.createGroupTextType(json);
								}

								/** Add triggerTo to JSON for later use */
								if (json.has(QuestionCons.TRIGGER_TO)) {
									try {
										String triggerTo = json.getString(QuestionCons.TRIGGER_TO);
										if (triggerTo != JSONObject.NULL && !triggerTo.equals("")) {

											ViewDataHolder holder = (ViewDataHolder) v.getTag();
											questionHelper.registerTrigger(holder.getVarname(),
													json.getString(QuestionCons.TRIGGER_TO));
										}
									} catch (JSONException e) {
										Log.e("handler background", e.toString());
									} catch (NullPointerException e) {
										Log.e(getClass().getSimpleName(), e.toString() + type);
									}
								}

								/* if answer not null don't hide question */
								boolean isAnswerAvailable = false;
								if (questionHelper.getAnswer().has(varName)) {
									try {
										String answer = questionHelper.getAnswer().getString(varName);
										if (!answer.equalsIgnoreCase(""))
											isAnswerAvailable = true;
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}

								/* hide if item has a visible condition */
								if (json.has(QuestionCons.CONDITION) && !isAnswerAvailable) {
									try {
										if (json.get(QuestionCons.CONDITION) != JSONObject.NULL) {
											v.setVisibility(View.GONE);
											Log.d("condition", json.getString(QuestionCons.CONDITION));
										}
									} catch (JSONException e) {
										Log.e("hideItem", e.toString());
									}
								}

								llVertical.addView(v);
							}
						});
					}
				} catch (Exception e) {
					Log.e("doInBackgroud renderForm", e.toString());
				}
				return null;
			}

			protected void onPostExecute(Void result) {
				pd.dismiss();
			};

		}.execute();

		return llVertical;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			int type = Integer.parseInt(String.valueOf(requestCode).substring(0, 1));
			requestCode = Integer.parseInt(String.valueOf(requestCode).substring(1));

			if (type == RequestConstant.REQUEST_PHOTO) {
				try {
					Bitmap bmp;
					ImageView img = (ImageView) formBuilder.listOfView.get(requestCode);

					boolean isFromExtra = data.getExtras() == null ? false : true;
					if (isFromExtra) {
						bmp = (Bitmap) data.getExtras().get("data");
					} else {
						bmp = ImageHelper.getBitmapFromUri(this, data.getData());
						float degree = ImageHelper.rotationForImage(getApplicationContext(), data.getData());
						bmp = ImageHelper.rotate(bmp, (int) degree);
					}

					try {
						new File(questionHelper.answer.getString(img.getTag().toString())).delete();
					} catch (Exception e) {
						Log.w("get photo path", e.toString());
					}

					File f = StorageManager.getImageFile2(mSubDir, img.getTag().toString());
					FileOutputStream out = new FileOutputStream(f);
					bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
					img.setImageBitmap(bmp);

					Debug.i(img.getTag().toString());
					
					questionHelper.inputData(img.getTag().toString(), f.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("get imageview", e.toString());
				}
			} else if (type == RequestConstant.REQUEST_SIGNATURE) {
				try {
					ImageView img = (ImageView) formBuilder.listOfView.get(requestCode);
					Bitmap bmp = (Bitmap) data.getExtras().getParcelable(FingerPaint.SIGNATURE);
					img.setImageBitmap(bmp);

					try {
						new File(questionHelper.answer.getString(img.getTag().toString())).delete();
					} catch (Exception e) {
						Log.w("get signature path", e.toString());
					}

					File f = StorageManager.getSignatureFile(mSubDir);
					FileOutputStream out = new FileOutputStream(f);
					bmp.compress(Bitmap.CompressFormat.PNG, 90, out);

					questionHelper.inputData(img.getTag().toString(), f.getPath());
				} catch (Exception e) {
					Log.e("get signature view", e.toString());
				}
			} else if (type == RequestConstant.REQUEST_ATTACHMENT) {
				try {
					Random r = new Random(10000);
					EditText inpJawaban = (EditText) formBuilder.listOfView.get(requestCode);

					if (!TextUtils.isEmpty(inpJawaban.getText().toString())) {
						new File(inpJawaban.getText().toString()).delete();
					}

					File path = new File(StorageManager.getPath(mSubDir));
					File source = new File(data.getData().getPath());

					Log.d("myDataPath", path.getAbsolutePath());
					Log.d("dataPath", source.getAbsolutePath());

					String extension = source.getName().substring(source.getName().length() - 4);
					String filename = source.getName().substring(0, source.getName().length() - 4)
							+ r.nextInt();
					File outputFile = new File(path, filename + extension);
					path.mkdirs();
					outputFile.createNewFile();
					InputStream is = new FileInputStream(source);
					OutputStream os = new FileOutputStream(outputFile);
					StorageManager.copyFile(is, os);
					inpJawaban.setText(outputFile.getPath());

					questionHelper.inputData(inpJawaban.getTag().toString(), outputFile.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("get data path ", e.toString());
				}
			} else if (type == RequestConstant.REQUEST_CODE_SCAN) {
				try {
					EditText inpJawaban = (EditText) formBuilder.listOfView.get(requestCode);
					inpJawaban.setText(data.getStringExtra("SCAN_RESULT"));

					questionHelper.inputData(inpJawaban.getTag().toString(), inpJawaban.getText().toString());
				} catch (Exception e) {
					Log.e("get scan result", e.toString());
				}
			}
		} else {
			Log.w("activity result", "data null");
		}
	}

	/* ===================== Function ========================== */

	/** Method yang digunakan untuk mengirim jawaban berfomat JSON */
	/** yang bertipe data String */
	/** Request dilakukan secara async */

	private void onSend() {

		final ProgressDialog pd = new ProgressDialog(FormViewerActivity.this);
		pd.setMessage("Please Wait...");
		pd.show();

		int count = 0;

		JSONObject json = questionHelper.getAnswer();
		for (int i = 0; i < data.length(); i++) {
			try {
				JSONObject jason = data.getJSONObject(i);
				if (!json.getString(jason.getString(QuestionCons.VARIABLE)).equals("")) {
					count++;
				}
			} catch (Exception e) {
				Log.e("onSubmit", e.toString());
			}
		}

		/* if data empty, return */
		if (count == 0) {
			Toast.makeText(getApplication(), "Data harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
			return;
		}

		/* get config data from intent */
		Bundle b = getIntent().getExtras();
		final String siteID = b.getString(ConfigModel.SITE_CODE);
		ConfigModel config = new ConfigModel(b.getString(ConfigModel.PROVIDER),
				b.getString(ConfigModel.SITE_CODE), b.getString(ConfigModel.DOKUMEN),
				b.getString(ConfigModel.JOB));

		String url = String.format(URLCons.POST_DATA, config.getSiteCode(), config.getDokumen(), config.getProvider());

		RequestParams rParams = new RequestParams();
		rParams.put("", questionHelper.getAnswer().toString());

		NexwaveClient.post(FormViewerActivity.this, url, rParams, new AsyncHttpResponseHandler() {
			public void onSuccess(String content) {
				Log.d("post response", content);
				pd.dismiss();

				if (content == null) {
					onSendFailed();
					return;
				}

				try {
					JSONObject json = new JSONObject(content);
					String status = json.getString(NexwaveClient.STATUS);

					if (status.equalsIgnoreCase(NexwaveClient.STATUS_OK)) {
						showDialogMessageSuccess("Status", "Input data berhasil !", FormViewerActivity.this, UploadPhotoActivity.class, siteID, mSubDir);
						//startActivity(new Intent(FormViewerActivity.this, TrackingActivity.class));
						//Toast.makeText(getApplication(), "Input data berhasil !", Toast.LENGTH_LONG).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			};

			public void onFailure(Throwable error) {
				Log.e("error", error.toString());
				onSendFailed();
			};
		});
	}

	/** Callback yang dipanggil bila request gagal */
	private void onSendFailed() {
		Toast.makeText(getApplication(), "Pengiriman data gagal", Toast.LENGTH_SHORT).show();
	}

	/* ===================== Helper Method ====================== */

	/** Set subtitle actionbar menjadi template yang sedang dipilih */
	/** Misal : Telkomsel/SF/DCS COLO */
	private void initHeader() {

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Pertanyaan");

		Bundle b = getIntent().getExtras();

		String separator = "/";
		ConfigModel model = Utils.fastDecodeConfig(new ConfigModel(b.getString(ConfigModel.PROVIDER), b
				.getString(ConfigModel.SITE_CODE), b.getString(ConfigModel.DOKUMEN)));

		String indicator = model.getProvider() + separator + model.getSiteCode() + separator
				+ model.getDokumen();

		getSupportActionBar().setSubtitle(indicator);
	}

	/* ===================== Function =========================== */

	/**
	 * Menyimpan jawaban dalama format JSON yang beripe data String ke
	 * SharedPreference
	 */

	private void saveAnswer() {
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage("Menyimpan...");
		pd.setCancelable(false);
		pd.show();

		/* save answer */
		OpenData.saveAnswerData(getApplication(), templateID, questionHelper.getAnswer().toString());

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				pd.dismiss();
				finish();
			}
		}, 500);

	}
	
	/** Tambahan Si Gue 
	
	private void checkForm(int offset, int limit){
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage("Check form...");
		pd.setCancelable(false);
		pd.show();
		
		try{
			for (int x = offset; x < limit + 1; x++) {
				final JSONObject json = data.getJSONObject(x);
				final String varName = json.getString(QuestionCons.VARIABLE);
				
				if(questionHelper.getAnswer(varName).equals("null")){
					pd.dismiss();
					Toast.makeText(getApplicationContext(), "Masih ada yang kosong!", Toast.LENGTH_SHORT).show();	
					break;
				}
			}
		}catch (Exception e) {
			Log.e("Cek Answer All", e.toString());
		}
		
		Log.i("Cek Answer", questionHelper.getAnswer().toString());
	}
	 **/
	
	/* ===================== Interface ========================== */

	@Override
	public void onClick(View v) {
		saveAnswer();
	}
	
	/* ===================== Tambahan SiGue ========================== */
	
	@Override
	public void onBackPressed() {
		confirmDialog();
	}
	
	private void confirmDialog(){
		Builder builder = new AlertDialog.Builder(this);

		builder.setMessage("Anda ingin keluar dari form ini? pastikan anda telah menyimpan data sebelum keluar")
	  	  	   .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	  	  		@Override
	  	  		public void onClick(DialogInterface dialog, int which) {
					finish();
	  	  		}
	  	    })
	  	    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	  	  		@Override
	  	  		public void onClick(DialogInterface dialog, int which) {
	  	  			dialog.dismiss();
	  	  		}
	  	    });
	  	  	
		builder.create().show();
	}
}
