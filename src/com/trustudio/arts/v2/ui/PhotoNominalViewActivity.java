package com.trustudio.arts.v2.ui;

import com.trustudio.arts.v2.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class PhotoNominalViewActivity extends BaseActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_view);
		
		Bundle bundle 	= getIntent().getExtras();
		String path		= bundle.getString("com.photo");

		ImageView mImage= (ImageView) findViewById(R.id.full_image_view);
		
		Bitmap bmp 		= BitmapFactory.decodeFile(path);
		
		mImage.setImageBitmap(bmp);
	}
}
